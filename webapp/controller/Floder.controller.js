sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/BusyIndicator",
	"sap/ui/core/routing/History"
], function(Controller,BusyIndicator,History) {
	"use strict";

	return Controller.extend("nnext.iq.Workflow.controller.Floder", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nnext.iq.Workflow.view.Floder
		 */
		onInit: function() {
			var ctrl = this;

			ctrl._oRouter = sap.ui.core.UIComponent.getRouterFor(ctrl);
			
			$.ajax("/Flow7Api/api/diagram")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "folder");
				});
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nnext.iq.Workflow.view.Floder
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nnext.iq.Workflow.view.Floder
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nnext.iq.Workflow.view.Floder
		 */
		//	onExit: function() {
		//
		//	}
		onFolderPress: function(oEvent) {
			var ctrl = this;
			var oObject = oEvent.getSource().getBindingContext("folder");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			ctrl._oRouter.navTo("list", {
				FolderGuid: oItem.FolderGuid
			});
		}
	});

});